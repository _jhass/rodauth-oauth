# frozen_string_literal: true

module Rodauth
  module OAuth
    VERSION = "1.6.1"
  end
end
